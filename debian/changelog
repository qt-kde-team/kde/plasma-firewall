plasma-firewall (6.3.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.1).
  * New upstream release (6.3.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 28 Feb 2025 00:52:15 +0100

plasma-firewall (6.3.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 10 Feb 2025 14:59:10 +0100

plasma-firewall (6.2.91-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.91).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 23 Jan 2025 23:52:00 +0100

plasma-firewall (6.2.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.90).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 11 Jan 2025 23:37:55 +0100

plasma-firewall (6.2.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.5).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 05 Jan 2025 10:58:02 +0100

plasma-firewall (6.2.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 03 Dec 2024 16:31:20 +0100

plasma-firewall (6.2.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.3).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 23 Nov 2024 21:57:28 +0100

plasma-firewall (6.2.1-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.1).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 15 Oct 2024 17:49:27 +0200

plasma-firewall (6.2.0-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.90).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (6.2.0).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 05 Oct 2024 23:17:44 +0200

plasma-firewall (6.1.5-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 11 Sep 2024 23:27:49 +0200

plasma-firewall (6.1.4-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.4).
  * Review copyright information.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 11 Aug 2024 23:58:20 +0200

plasma-firewall (6.1.3-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.3).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 21 Jul 2024 23:46:16 +0200

plasma-firewall (6.1.0-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.1.0).
  * Update build-deps and deps with the info from cmake.

 -- Patrick Franz <deltaone@debian.org>  Tue, 18 Jun 2024 19:52:00 +0200

plasma-firewall (5.27.11-1) unstable; urgency=medium

  [ Patrick Franz ]
  * Bump Standards-Version to 4.7.0 (No changes needed).
  * New upstream release (5.27.11).
  * Remove inactive uploaders, thanks for your work.

 -- Patrick Franz <deltaone@debian.org>  Sun, 19 May 2024 12:20:55 +0200

plasma-firewall (5.27.10-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.10).

 -- Patrick Franz <deltaone@debian.org>  Thu, 11 Jan 2024 23:21:03 +0100

plasma-firewall (5.27.9-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.9).

 -- Patrick Franz <deltaone@debian.org>  Fri, 27 Oct 2023 21:51:35 +0200

plasma-firewall (5.27.8-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.8).

 -- Patrick Franz <deltaone@debian.org>  Wed, 13 Sep 2023 20:47:22 +0200

plasma-firewall (5.27.7-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.7).

 -- Patrick Franz <deltaone@debian.org>  Thu, 03 Aug 2023 18:50:14 +0200

plasma-firewall (5.27.5-2) unstable; urgency=medium

  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 27 May 2023 18:23:42 +0200

plasma-firewall (5.27.5-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.5).

 -- Patrick Franz <deltaone@debian.org>  Tue, 09 May 2023 23:23:19 +0200

plasma-firewall (5.27.3-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.3).

 -- Patrick Franz <deltaone@debian.org>  Wed, 22 Mar 2023 23:16:01 +0100

plasma-firewall (5.27.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.27.1).
  * New upstream release (5.27.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 28 Feb 2023 14:59:50 +0100

plasma-firewall (5.27.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.27.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 18 Feb 2023 17:08:39 +0100

plasma-firewall (5.26.90-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.90).
  * Bump Standards-Version to 4.6.2, no change required.
  * Run tests under xvfb, a graphical environment is required.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 23 Jan 2023 21:50:54 +0100

plasma-firewall (5.26.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 07 Jan 2023 00:20:56 +0100

plasma-firewall (5.26.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 29 Nov 2022 16:01:01 +0100

plasma-firewall (5.26.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 08 Nov 2022 15:43:33 +0100

plasma-firewall (5.26.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.1).
  * New upstream release (5.26.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 27 Oct 2022 23:30:54 +0200

plasma-firewall (5.26.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 11 Oct 2022 15:44:17 +0200

plasma-firewall (5.25.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.90).
  * Update build-deps and deps with the info from cmake.
  * Review copyright information.
  * Refresh lintian overrides.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 25 Sep 2022 00:14:22 +0200

plasma-firewall (5.25.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 09 Sep 2022 23:21:10 +0200

plasma-firewall (5.25.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 02 Aug 2022 17:31:35 +0200

plasma-firewall (5.25.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.2).
  * Drop unused build dependencies on libkf5plasma-dev and
    libqt5x11extras5-dev.
  * Add dependency to systemsettings for the KCM module.
  * New upstream release (5.25.3).
  * Release to unstable

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 17 Jul 2022 15:26:57 +0200

plasma-firewall (5.25.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.25.1).

 -- Patrick Franz <deltaone@debian.org>  Tue, 21 Jun 2022 21:45:20 +0200

plasma-firewall (5.25.0-1) experimental; urgency=medium

  * New upstream release (5.25.0).
  * Bump Standards-Version to 4.6.1, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 14 Jun 2022 21:26:23 +0200

plasma-firewall (5.24.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.90).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 20 May 2022 11:44:41 +0200

plasma-firewall (5.24.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 12 May 2022 21:39:33 +0200

plasma-firewall (5.24.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 30 Mar 2022 13:45:39 +0200

plasma-firewall (5.24.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.3).
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 10 Mar 2022 07:47:13 +0100

plasma-firewall (5.24.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.24.2).
  * Bump Standards-Version to 4.6.0 (no changes needed).
  * Re-export signing key without extra signatures.
  * Update d/copyright.

 -- Patrick Franz <deltaone@debian.org>  Sat, 26 Feb 2022 21:59:27 +0100

plasma-firewall (5.23.5-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.23.5).

 -- Patrick Franz <deltaone@debian.org>  Fri, 07 Jan 2022 15:44:38 +0100

plasma-firewall (5.23.4-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.4).

 -- Patrick Franz <deltaone@debian.org>  Thu, 02 Dec 2021 20:25:15 +0100

plasma-firewall (5.23.3-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.3).

 -- Norbert Preining <norbert@preining.info>  Wed, 10 Nov 2021 08:41:21 +0900

plasma-firewall (5.23.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * Update upstream signing-key.

  [ Norbert Preining ]
  * New upstream release (5.23.1).
  * New upstream release (5.23.2).

 -- Norbert Preining <norbert@preining.info>  Wed, 03 Nov 2021 22:21:03 +0900

plasma-firewall (5.23.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.0).

 -- Norbert Preining <norbert@preining.info>  Thu, 14 Oct 2021 20:13:15 +0900

plasma-firewall (5.21.5-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Release to unstable.

 -- Patrick Franz <deltaone@debian.org>  Tue, 17 Aug 2021 03:24:55 +0200

plasma-firewall (5.21.5-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.5).

 -- Patrick Franz <patfra71@gmail.com>  Fri, 07 May 2021 13:55:18 +0200

plasma-firewall (5.21.4-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.4).

 -- Patrick Franz <patfra71@gmail.com>  Tue, 06 Apr 2021 17:47:48 +0200

plasma-firewall (5.21.3-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.3).

 -- Norbert Preining <norbert@preining.info>  Wed, 17 Mar 2021 05:49:42 +0900

plasma-firewall (5.21.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.2).

 -- Norbert Preining <norbert@preining.info>  Wed, 03 Mar 2021 05:33:50 +0900

plasma-firewall (5.21.1-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.1).

 -- Norbert Preining <norbert@preining.info>  Wed, 24 Feb 2021 14:36:40 +0900

plasma-firewall (5.21.0-1) experimental; urgency=medium

  * New upstream release.
  * Switch watch file to the stable releases.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Pino Toscano <pino@debian.org>  Tue, 16 Feb 2021 14:13:45 +0100

plasma-firewall (5.20.90-1) experimental; urgency=medium

  * Initial packaging.

 -- Pino Toscano <pino@debian.org>  Wed, 03 Feb 2021 23:35:31 +0100
